//
//  MDMSDKAppDelegate.h
//  TestApp
//
//  Created by Ranjan on 9/26/13.
//  Copyright (c) 2013 Orion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MDMSDKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

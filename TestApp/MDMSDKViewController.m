//
//  MDMSDKViewController.m
//  TestApp
//
//  Created by Ranjan on 9/26/13.
//  Copyright (c) 2013 Orion. All rights reserved.
//

#import "MDMSDKViewController.h"

@interface MDMSDKViewController ()

@end

@implementation MDMSDKViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIView *adunitView = [[UIView alloc] initWithFrame: CGRectMake ( 22, 68, 280, 380)];
    //add code to customize, e.g. polygonView.backgroundColor = [UIColor blackColor];
    adunitView.backgroundColor = [UIColor grayColor];
    [self.view addSubview:adunitView];
    [adunitView release];
	
    
    /*  Block of code for Claim your Reward  ( Design Block)   */
    
    // Do any additional setup after loading the view.
    UITextField* tbclaim = [[UITextField alloc] initWithFrame:CGRectMake(100.0f, 318.0f,150.0f, 33.0f)];
    tbclaim.delegate = self;
    //changes the border style so the text field appears on screen
    tbclaim.borderStyle = UITextBorderStyleRoundedRect;

    [adunitView addSubview:tbclaim];
    
    
    //Facebook Button
    UIButton *btnfb = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    //set the button's frame
    btnfb.frame = CGRectMake(1.0f, 318.0f, 33.0f, 33.0f);
    
    //set the target, action, and control event.
    //more about this in the paragraph below
    [btnfb addTarget:self action:@selector(buttonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    //set the title
    //[btnfb setTitle:@"F!" forState:UIControlStateNormal];
    
    [btnfb setBackgroundImage:[UIImage imageNamed:@"btn_facebook.png"] forState:UIControlStateNormal];

    //add the button to the main view
    [adunitView addSubview:btnfb];
    
    //Twitter Button
    UIButton *btntw = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    //set the button's frame
    btntw.frame = CGRectMake(33.0f, 318.0f, 33.0f, 33.0f);
    
    //set the target, action, and control event.
    //more about this in the paragraph below
    [btntw addTarget:self action:@selector(buttonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    //set the title
    //[btntw setTitle:@"T!" forState:UIControlStateNormal];
    [btntw setBackgroundImage:[UIImage imageNamed:@"twitter.png"] forState:UIControlStateNormal];
    
    //add the button to the main view
    [adunitView addSubview:btntw];
    
    
    //Linkedin Button
    UIButton *btnlnkd = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    //set the button's frame
    btnlnkd.frame = CGRectMake(66.0f, 318.0f, 33.0f, 33.0f);
    
    //set the target, action, and control event.
    //more about this in the paragraph below
    [btnlnkd addTarget:self action:@selector(buttonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    //set the title
    //[btnlnkd setTitle:@"L!" forState:UIControlStateNormal];
    [btnlnkd setBackgroundImage:[UIImage imageNamed:@"linkedin.png"] forState:UIControlStateNormal];
    
    //add the button to the main view
    [adunitView addSubview:btnlnkd];
    
    
    //Button for Go button
    UIButton *btngo = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    //set the button's frame
    btngo.frame = CGRectMake(246.0f, 318.0f, 33.0f, 33.0f);
    
    //set the target, action, and control event.
    //more about this in the paragraph below
    [btngo addTarget:self action:@selector(buttonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    //set the title
    //[btngo setTitle:@"Go" forState:UIControlStateNormal];
     [btngo setBackgroundImage:[UIImage imageNamed:@"go.png"] forState:UIControlStateNormal];
    
    //add the button to the main view
    [adunitView addSubview:btngo];
    

    //Button for close ad unit
    UIButton *btnclose = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    //set the button's frame
    btnclose.frame = CGRectMake(259.0f, 10.0f, 20.0f, 20.0f);
    
    //set the target, action, and control event.
    //more about this in the paragraph below
    [btnclose addTarget:self action:@selector(buttonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    //set the title
    //[btnclose setTitle:@"X" forState:UIControlStateNormal];
      [btnclose setBackgroundImage:[UIImage imageNamed:@"cancelEmail.png"] forState:UIControlStateNormal];
    
    
   
    //add the button to the main view
    [adunitView addSubview:btnclose];
    
    

    //Adunit Display-Webview
    
    UIWebView* adunit = [[UIWebView alloc] initWithFrame:CGRectMake(15.0f, 32.0f,250.0f, 250.0f)];
    [adunit setBackgroundColor:[UIColor clearColor]];
    [adunit setOpaque:NO];
    [adunitView addSubview:adunit];
    
    
     NSString *path =[[NSBundle mainBundle] bundlePath];
     NSURL *baseUrl =[NSURL fileURLWithPath:path];
    
     NSString *html = [NSString stringWithFormat:@"<html><head>"
    //THE LINE ABOVE IS TO PREVENT THE VIEW TO BE SCROLLABLE
    "<script>document.ontouchstart = function(event) { event.preventDefault();  }</script>"
     "</head><body style=\"text-align:center;margin:0;padding:0\">"
     "<img width=\"1000px;\" src=\"http://sachinahuja.com/demo/adv/images/portfolio06.jpg\" />"
     "</body>"
     "</html>"];
     //NSString *html = [NSString stringWithFormat:@"<html><head></head><body></body></html>"];
     adunit.scalesPageToFit = YES;
     [adunit loadHTMLString:html baseURL:baseUrl];
    
}
- (void)buttonPressed {
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

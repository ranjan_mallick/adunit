//
//  MDMSDKViewController.h
//  TestApp
//
//  Created by Ranjan on 9/26/13.
//  Copyright (c) 2013 Orion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MDMSDKViewController : UIViewController <UITextFieldDelegate>
@property (nonatomic, strong) UITextField *tbclaim;
@property (nonatomic, strong) UILabel *label;
@end

//
//  main.m
//  TestApp
//
//  Created by Ranjan on 9/26/13.
//  Copyright (c) 2013 Orion. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MDMSDKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MDMSDKAppDelegate class]));
    }
}
